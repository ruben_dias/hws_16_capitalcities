//
//  Capital.swift
//  Project16
//
//  Created by Ruben Dias on 17/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import MapKit
import UIKit

class Capital: NSObject, MKAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var info: String

    init(title: String, coordinate: CLLocationCoordinate2D, info: String) {
        self.title = title
        self.coordinate = coordinate
        self.info = info
    }
}
